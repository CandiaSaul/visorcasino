﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Printing

Public Class VisorCasino

    Dim CodInterno, APE_PAT, APE_MAT, Nombres, Empresa, SQL, SQLArt22, cadena, Asiste, TmEntreComidas As String
    Dim dt As New DataTable
    Dim menuElegido, AutorizaMenuHipo, cmd As String
    Dim lector, LectorArt22, Lectorfinal, Prueba As SqlDataReader
    Dim lectorOffline, lectorFinalOffline As OleDbDataReader
    Private oskProcess As Process
    Dim sConnectionLocalHost As String = "Provider=SQLOLEDB;Data Source=.\SQLEXPRESS;Initial Catalog=BD_FORESTALLEONERA;Integrated Security=SSPI"


    Private Sub BtnConectarRed_Click(sender As Object, e As EventArgs) Handles BtnConectarRed.Click
        cmd = "explorer.exe ms-availablenetworks:"
        Shell(cmd)
    End Sub

    Private Sub BtnRestartApp_Click(sender As Object, e As EventArgs) Handles BtnRestartApp.Click
        Application.Restart()
    End Sub

    Private Sub CtxImprimirResumen_Click(sender As Object, e As EventArgs) Handles CtxImprimirResumen.Click
        ImpresionResumen.Print()
    End Sub

    Private Sub BtnReiniciar_Click(sender As Object, e As EventArgs) Handles BtnReiniciar.Click
        Dim cmd = "shutdown /r /t 0"
        Shell(cmd)
        Close()
    End Sub

    Private Sub CtxMinimizar_Click(sender As Object, e As EventArgs) Handles CtxMinimizar.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Dim tipoconexion As Cadenas = New Cadenas()

    Private Sub chk_ModoOffline_Click(sender As Object, e As EventArgs) Handles chk_ModoOffline.Click
        If chk_ModoOffline.Checked = False Then
            Dim confirmarOffline As DialogResult
            confirmarOffline = MessageBox.Show("Al desactivar el modo OFFLINE, se establecerá conexión con la red y la base de datos." + Environment.NewLine + Environment.NewLine + "Esto tomará unos segundos por lo que el sistema podría no responder en ese lapso." + Environment.NewLine + Environment.NewLine + "¿Desea continuar?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If confirmarOffline = vbYes Then
                If PanelLoading.Visible = False Then
                    PanelLoading.Visible = True
                End If
                lblCargando.Text = "Intentando conectar..."

                'Realiza una pequeña prueba de conexión con servicio SQL antes de pasar a modo OnLine
                'Si no logra conectar, aplicación continuará en modo OFFLINE
                cadena = tipoconexion.EscogeConexion()
                Dim conexion As New SqlConnection(cadena)
                SQL = "SELECT 1 from REGISTROCASINOS"
                Dim comando2 As New SqlCommand(SQL, conexion)
                Try
                    conexion.Open()
                    Prueba = comando2.ExecuteReader
                    ModoOffline = False
                    conexion.Close()
                    PanelLoading.Visible = False
                    MessageBox.Show("Sistema conectado (ONLINE).", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    chk_ModoOffline.Checked = False
                    lblStatus.ForeColor = System.Drawing.Color.Green
                    lblStatus.Text = "Sistema conectado a Internet (ONLINE)"
                    PanelLoading.Visible = False
                    CodInterno = ""
                    lblNombre.Text = ""
                    lblEmpresa.Text = ""
                    lblFecha.Text = ""
                    lblmensaje.Text = ""
                    lblmensaje2.Text = ""
                Catch ex As Exception
                    PanelLoading.Visible = False
                    ModoOffline = True
                    MessageBox.Show("El intento de conexión no dio resultados. Conéctese a una red e inténtelo de nuevo" + Environment.NewLine + Environment.NewLine + "El sistema continuará trabajando en modo OFFLINE", "No se pudo conectar", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    chk_ModoOffline.Checked = True
                    lblStatus.ForeColor = System.Drawing.Color.Blue
                    lblStatus.Text = "Sistema operando en modo OFFLINE (Sin conexión)"
                    CodInterno = ""
                    lblNombre.Text = ""
                    lblEmpresa.Text = ""
                    lblFecha.Text = ""
                    lblmensaje.Text = ""
                    lblmensaje2.Text = ""
                End Try
            End If
        Else
            PanelLoading.Visible = False
            ModoOffline = True
            chk_ModoOffline.Checked = True
            MessageBox.Show("Modo Sin Conexión (OFFLINE) Activado", "Modo OFFLINE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            lblStatus.ForeColor = System.Drawing.Color.Blue
            lblStatus.Text = "Sistema operando en modo OFFLINE (Sin conexión)"
            CodInterno = ""
            lblNombre.Text = ""
            lblEmpresa.Text = ""
            lblFecha.Text = ""
            lblmensaje.Text = ""
            lblmensaje2.Text = ""
        End If
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub BtnOSK_Click(sender As Object, e As EventArgs) Handles BtnOSK.Click
        Try
            If oskProcess Is Nothing OrElse oskProcess.HasExited Then
                If oskProcess IsNot Nothing AndAlso oskProcess.HasExited Then
                    oskProcess.Close()
                End If
                'oskProcess = Process.Start("C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe")
                oskProcess = Process.Start("C:\Numpad\Numpad.exe")
                'give the focus to the textbox
                TxtRut.Focus()
                BtnOSK.Text = "Esconder teclado en pantalla"
                TxtRut.Focus()
            Else
                For Each pkiller As Process In Process.GetProcesses
                    If String.Compare(pkiller.ProcessName, "Numpad", True) = 0 Then
                        pkiller.Kill()
                        BtnOSK.Text = "Mostrar teclado en pantalla"
                    End If
                Next
            End If
        Catch ex As Exception
            MessageBox.Show("El complemento no está instalado o no fue encontrado.", "No se puede iniciar teclado", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnReports_Click_1(sender As Object, e As EventArgs) Handles btnReports.Click
        'Dim oForm As New Estadisticas

        ''oForm.Show()

        'If Not oForm.Visible Then
        '    oForm.Show()
        'Else
        '    If oForm.WindowState = FormWindowState.Minimized Then
        '        oForm.WindowState = FormWindowState.Maximized
        '        oForm.Activate()

        '    End If
        'End If
        System.Diagnostics.Process.Start("https://app.powerbi.com/reportEmbed?reportId=f8373a0f-2a86-472b-86d6-8fecc5bb8904&autoAuth=true&ctid=70de5114-582f-4f85-a1a5-b2bf39704f0a&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXBhYXMtMS1zY3VzLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9")

    End Sub

    Dim mensaje As String
    Dim recuentoPlatos As Integer
    Dim trabajadorExiste As String
    Dim CuentaForestal, CuentaPaneles, CuentaTransportes, CuentaHolding, CuentaOtros

    Private Sub ProcesaAsistencia()

        cadena = tipoconexion.EscogeConexion()
        Dim conexion As New SqlConnection(cadena)
        Dim sqlFinal As String
        cadena = tipoconexion.EscogeConexion()


        Dim conexionN As New SqlConnection(cadena)
        If trabajadorExiste = "S" Then
            sqlFinal = "SELECT DATEDIFF(hh,FechaColacion,getdate()) as Diferencia " &
                  "from REGISTROCASINOS " &
                  "WHERE (COD_INTERNO=@CodInterno)  anD (CONVERT(VarChar(13),FechaColacion,103))=CONVERT(VarChar(13),GETDATE(),103)  " &
                  "order by FechaColacion desc"
            Dim comandoN As New SqlCommand(sqlFinal, conexionN)
            comandoN.Parameters.Add("@CodInterno", SqlDbType.NChar).Value = CodInterno

            Try
                conexionN.Open()
                Lectorfinal = comandoN.ExecuteReader

                If Lectorfinal.Read Then
                    If Lectorfinal(0) >= 4 Then
                        TmEntreComidas = "S"
                    Else
                        TmEntreComidas = "N"

                    End If
                Else
                    TmEntreComidas = "S"
                End If

            Catch ex As Exception
                lblmensaje.Text = ex.Message.ToString
                'MsgBox("Registro NO ingresado correctamente")
            End Try
        Else


        End If

        If trabajadorExiste = "S" And TmEntreComidas = "S" Then

            'Si está todo ok, se imprime ticket y guarda datos en base de datos
            InsertaDatos(CodInterno, menuElegido)
            HojaImpresion.Print()

            Select Case Empresa
                Case "FORESTAL Y ASERRDAROS LEONERA "
                    CuentaForestal = CuentaForestal + 1

                Case "PANELES LEONERA LTDA."
                    CuentaPaneles = CuentaPaneles + 1

                Case "TRANSPORTES LEONERA LTDA."
                    CuentaTransportes = CuentaTransportes + 1

                Case "HOLDING LEONERA S.A."
                    CuentaHolding = CuentaHolding + 1

                Case Else
                    CuentaOtros = CuentaOtros + 1
            End Select



            lblCuentaForestal.Text = "Colaciones por Forestal: " & CuentaForestal
            lblCuentaPaneles.Text = "Colaciones por Paneles: " & CuentaPaneles
            lblCuentaTransportes.Text = "Colaciones por Transportes: " & CuentaTransportes
            lblCuentaHolding.Text = "Colaciones por Holding: " & CuentaHolding
            lblCuentaOtros.Text = "Colaciones Otros: " & CuentaOtros
            lblTotal.Text = "Colaciones Totales: " & CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros

            ProgressBarForestal.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarForestal.Value = CuentaForestal

            ProgressBarPaneles.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarPaneles.Value = CuentaPaneles

            ProgressBarTransportes.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarTransportes.Value = CuentaTransportes

            ProgressBarHolding.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarHolding.Value = CuentaHolding

            ProgressBarOtros.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarOtros.Value = CuentaOtros

            CodInterno = ""
            TxtRut.Text = ""

        Else
            If trabajadorExiste = "N" Then
                lblmensaje.Text = "Colación no autorizada."
                lblmensaje2.Text = "Trabajador no registrado en sistema"
                CodInterno = ""

            ElseIf TmEntreComidas = "N" Then
                lblmensaje.Text = "Colación no autorizada."
                lblmensaje2.Text = "Ha pasado menos de 4 horas desde la última colación."
                CodInterno = ""
                TxtRut.Text = ""

            End If

        End If

    End Sub

    Private Sub ProcesaAsistenciaOffline()
        'Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\DB.mdb;Persist Security Info=False;"
        Dim sConnectionString As String = sConnectionLocalHost
        Dim Conn As New OleDbConnection(sConnectionString)

        If trabajadorExiste = "S" Then
            SQL = "select DATEDIFF(hh,FechaColacion,GETDATE()) from REGISTROCASINOS where cod_interno = '" + CodInterno.ToString + "'"
            Dim comandoOffline As New OleDbCommand(SQL, Conn)

            Try
                Conn.Open()
                lectorFinalOffline = comandoOffline.ExecuteReader

                If lectorFinalOffline.Read Then
                    If lectorFinalOffline(0) >= 4 Then
                        TmEntreComidas = "S"
                    Else
                        TmEntreComidas = "N"
                    End If
                Else
                    TmEntreComidas = "S"
                End If
            Catch ex As Exception
                lblmensaje.Text = ex.Message.ToString
            End Try

        Else
        End If

        If trabajadorExiste = "S" And TmEntreComidas = "S" Then
            'Si está todo ok, se imprime ticket y guarda datos en base de datos LOCAL
            InsertaDatosOffline(CodInterno, "N")
            HojaImpresion.Print()

            Select Case Empresa
                Case "FORESTAL Y ASERRDAROS LEONERA "
                    CuentaForestal = CuentaForestal + 1

                Case "PANELES LEONERA LTDA."
                    CuentaPaneles = CuentaPaneles + 1

                Case "TRANSPORTES LEONERA LTDA."
                    CuentaTransportes = CuentaTransportes + 1

                Case "HOLDING LEONERA S.A."
                    CuentaHolding = CuentaHolding + 1

                Case Else
                    CuentaOtros = CuentaOtros + 1
            End Select

            lblCuentaForestal.Text = "Colaciones por Forestal: " & CuentaForestal
            lblCuentaPaneles.Text = "Colaciones por Paneles: " & CuentaPaneles
            lblCuentaTransportes.Text = "Colaciones por Transportes: " & CuentaTransportes
            lblCuentaHolding.Text = "Colaciones por Holding: " & CuentaHolding
            lblCuentaOtros.Text = "Colaciones Otros: " & CuentaOtros
            lblTotal.Text = "Colaciones Totales: " & CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros

            ProgressBarForestal.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarForestal.Value = CuentaForestal

            ProgressBarPaneles.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarPaneles.Value = CuentaPaneles

            ProgressBarTransportes.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarTransportes.Value = CuentaTransportes

            ProgressBarHolding.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarHolding.Value = CuentaHolding

            ProgressBarOtros.MaximumValue = (CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros)
            ProgressBarOtros.Value = CuentaOtros

            CodInterno = ""
            TxtRut.Text = ""

        Else
            If trabajadorExiste = "N" Then
                lblmensaje.Text = "Colación no autorizada."
                lblmensaje2.Text = "Trabajador no registrado en sistema."
                CodInterno = ""

            ElseIf TmEntreComidas = "N" Then
                lblmensaje.Text = "Colación no autorizada."
                lblmensaje2.Text = "Ha pasado menos de 4 horas desde la última colación."
                CodInterno = ""
                TxtRut.Text = ""

            End If

        End If



    End Sub



    Private Sub InsertaDatos(ByVal CodInterno As String, ByVal TipoColacion As String)

        cadena = tipoconexion.EscogeConexion()
        Dim conexion As New SqlConnection(cadena)


        SQL = "INSERT INTO REGISTROCASINOS (COD_INTERNO, FechaColacion, TipoColacion, TipoCasino) " &
            "VALUES (@COD_INTERNO, GETDATE(), @TipoColacion, @TipoCasino)"
        Dim comando As New SqlCommand(SQL, conexion)
        comando.Parameters.Add("@COD_INTERNO", SqlDbType.NChar).Value = CodInterno
        comando.Parameters.Add("@FechaColacion", SqlDbType.NChar).Value = DateTime.Now
        comando.Parameters.Add("@TipoColacion", SqlDbType.NChar).Value = TipoColacion
        comando.Parameters.Add("@TipoCasino", SqlDbType.NChar).Value = Casinoelegido
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            ' MsgBox("Registro ingresado correctamente")
            mensaje = "OK"

        Catch ex As Exception
            lblmensaje.Text = ex.Message.ToString
            mensaje = "ERROR"
        End Try
    End Sub

    Private Sub InsertaDatosOffline(ByVal CodInterno As String, ByVal TipoColacion As String)

        Dim sConnectionString As String = sConnectionLocalHost
        Dim Conn As New OleDbConnection(sConnectionString)

        SQL = "INSERT INTO REGISTROCASINOS(COD_INTERNO, FechaColacion, TipoColacion, TipoCasino) " &
            "VALUES ('" + CodInterno.ToString + "', GETDATE(), '" + TipoColacion.ToString + "', '" + Casinoelegido + "')"
        Dim comandoOffline As New OleDbCommand(SQL, Conn)

        Try
            Conn.Open()
            comandoOffline.ExecuteNonQuery()
            Conn.Close()
            'MsgBox("Registro ingresado correctamente")
            lblmensaje.Text = ""
            lblmensaje2.Text = ""
            mensaje = "OK"

        Catch ex As Exception
            lblmensaje.Text = ex.Message.ToString
            mensaje = "ERROR"
        End Try
    End Sub



    Private Sub HojaImpresion_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles HojaImpresion.PrintPage

        Try
            Dim prFont As New Font("Arial", 14, FontStyle.Bold)
            'la posición superior

            Me.pbLogoTicket.Size = New System.Drawing.Size(10, 10)
            Me.pbLogoTicket.SizeMode = PictureBoxSizeMode.StretchImage
            e.Graphics.DrawImage(pbLogoTicket.Image, 0, 0, 100, 50)

            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("CASINO GRUPO LEONERA", prFont, Brushes.Black, 100, 18)
            prFont = New Font("Arial", 8, FontStyle.Regular)
            e.Graphics.DrawString(Nombres + " " + APE_PAT + " " + APE_MAT, prFont, Brushes.Black, 40, 80)
            prFont = New Font("Arial", 8, FontStyle.Regular)
            e.Graphics.DrawString(DateTime.Now.ToString, prFont, Brushes.Black, 40, 100)
            prFont = New Font("Arial", 8, FontStyle.Regular)
            e.Graphics.DrawString(Empresa, prFont, Brushes.Black, 40, 120)
            prFont = New Font("Arial", 8, FontStyle.Regular)
            e.Graphics.DrawString(".: Desarrollo y Sistemas. 2022 CASV / SC :.", prFont, Brushes.Black, 6, 160)
            ' Fin de hoja
            e.HasMorePages = False

        Catch ex As Exception
            lblmensaje.Text = "ERROR: " & ex.Message

        End Try
    End Sub


    Private Sub ImpresionResumen_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles ImpresionResumen.PrintPage

        Try
            Dim prFont As New Font("Arial", 16, FontStyle.Bold)
            'la posición superior

            Me.pbLogoTicket.Size = New System.Drawing.Size(10, 10)
            Me.pbLogoTicket.SizeMode = PictureBoxSizeMode.StretchImage
            e.Graphics.DrawImage(pbLogoTicket.Image, 0, 0, 100, 50)

            prFont = New Font("Arial", 12, FontStyle.Regular)
            e.Graphics.DrawString("CASINO GRUPO LEONERA", prFont, Brushes.Black, 100, 10)
            e.Graphics.DrawString("Ticket Resumen de colaciones", prFont, Brushes.Black, 100, 30)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString(DateTime.Now.ToString, prFont, Brushes.Black, 60, 60)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("----------------------------------------------------", prFont, Brushes.Black, 20, 70)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("Colaciones HOLDING:      " & CuentaHolding, prFont, Brushes.Black, 30, 90)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("Colaciones FORESTAL:    " & CuentaForestal, prFont, Brushes.Black, 30, 110)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("Colaciones PANELES:      " & CuentaPaneles, prFont, Brushes.Black, 30, 130)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("Colaciones TRANSPORTES:  " & CuentaTransportes, prFont, Brushes.Black, 30, 150)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("Colaciones OTROS:        " & CuentaOtros, prFont, Brushes.Black, 30, 170)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("-------------------------------------------------------", prFont, Brushes.Black, 20, 190)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("TOTAL COLACIONES:        " & CuentaForestal + CuentaPaneles + CuentaTransportes + CuentaHolding + CuentaOtros, prFont, Brushes.Black, 40, 210)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString("-------------------------------------------------------", prFont, Brushes.Black, 20, 230)
            prFont = New Font("Arial", 10, FontStyle.Regular)
            e.Graphics.DrawString(".: Desarrollo y Sistemas. 2022 CASV / SC  :.", prFont, Brushes.Black, 10, 250)
            ' Fin de hoja
            e.HasMorePages = False

        Catch ex As Exception
            lblmensaje.Text = "ERROR: " & ex.Message

        End Try

    End Sub



    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If (MsgBox("¿Esta seguro de salir?", vbQuestion + vbYesNo) = vbYes) Then
            Application.Exit()
        End If
    End Sub

    Private Sub VisorCasino_Load(sender As Object, e As EventArgs) Handles Me.Load

        If ModoOffline = True Then
            lblStatus.ForeColor = System.Drawing.Color.Blue
            lblStatus.Text = "Sistema operando en modo OFFLINE (Sin conexión)"
            chk_ModoOffline.Checked = True
        Else
            lblStatus.ForeColor = System.Drawing.Color.Green
            lblStatus.Text = "Sistema conectado a Internet (ONLINE)"
            chk_ModoOffline.Checked = False
        End If

        lblCasinoIniciado.Text = Casinoelegido.ToString
        pbLogoTicket.Visible = False
        PanelLoading.Visible = False
        TxtRut.Select()

        CodInterno = ""
        lblNombre.Text = ""
        lblEmpresa.Text = ""
        lblFecha.Text = ""
        lblmensaje.Text = ""
        lblmensaje2.Text = ""

        CuentaForestal = 0
        CuentaPaneles = 0
        CuentaTransportes = 0
        CuentaHolding = 0
        CuentaOtros = 0

        lblCuentaForestal.Text = "Colaciones por Forestal: "
        lblCuentaPaneles.Text = "Colaciones por Paneles: "
        lblCuentaTransportes.Text = "Colaciones por Transportes: "
        lblCuentaHolding.Text = "Colaciones por Holding: "
        lblCuentaOtros.Text = "Colaciones Otros: "

    End Sub


    Private Sub TxtRut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtRut.KeyPress

        lblCargando.Text = "Cargando datos..."
        ProgressBarCargando.Style = ProgressBarStyle.Marquee
        ProgressBarCargando.Value = 80

        If PanelLoading.Visible = False Then
            PanelLoading.Visible = True
        End If


        If e.KeyChar = Chr(13) Then '(Char13) es ENTER

            If ModoOffline = True Then
                ProcesaDatosOffline()
                'MsgBox("Sistema OFFline")
            ElseIf ModoOffline = False Then
                'MsgBox("Sistema Oline")
                ProcesaDatosOnline()
            End If
        End If
    End Sub

    Private Sub ProcesaDatosOnline()
        'Procesa los datos en línea (OnLine)
        cadena = tipoconexion.EscogeConexion()
        Dim conexion As New SqlConnection(cadena)
        menuElegido = "N"
        lblmensaje.Text = ""
        lblmensaje2.Text = ""

        SQL = " SELECT T_EMPLEADO.COD_INTERNO, T_EMPLEADO.NOMBRES, T_EMPLEADO.APE_PAT, T_EMPLEADO.APE_MAT,P_EMPRESA.RAZON_SOCIAL, T_EMPLEADO.FOTO " &
              " From T_EMPLEADO LEFT OUTER Join  " &
              " P_EMPRESA On T_EMPLEADO.COD_EMPRESA = P_EMPRESA.COD_EMPRESA  " &
              " Where (RIGHT(T_EMPLEADO.TARJETA_NUMERO, 8) =  RIGHT('000'+@tarjeta,8)) OR (RIGHT(T_EMPLEADO.RUT, 8) =  RIGHT('000'+@tarjeta,8)) AND GETDATE() BETWEEN DATEADD(d,0,DATEDIFF(d,0,T_EMPLEADO.FECHA_CONTRATO)) AND T_EMPLEADO.FECHA_FIN_CONTRATO"
        '" Where (RIGHT(T_EMPLEADO.TARJETA_NUMERO, 8) =  @tarjeta) OR (RIGHT(T_EMPLEADO.RUT, 8) =  @tarjeta) AND GETDATE() BETWEEN T_EMPLEADO.FECHA_CONTRATO AND T_EMPLEADO.FECHA_FIN_CONTRATO"
        '" Where (Right(T_EMPLEADO.TARJETA_NUMERO, 8) =  @Tarjeta) AND GETDATE() BETWEEN T_EMPLEADO.FECHA_CONTRATO AND T_EMPLEADO.FECHA_FIN_CONTRATO"


        Dim comando2 As New SqlCommand(SQL, conexion)
        Dim pin As String = ""

        comando2.Parameters.Add("@Tarjeta", SqlDbType.NChar).Value = TxtRut.Text
        Try
            conexion.Open()
            lector = comando2.ExecuteReader

            If lector.Read Then
                CodInterno = lector(0)
                Nombres = lector(1)
                APE_PAT = lector(2)
                APE_MAT = lector(3)
                Empresa = lector(4)
                trabajadorExiste = "S"

                lblNombre.Text = lector(1) & " " & lector(2) & " " & lector(3)
                lblEmpresa.Text = Empresa
                lblFecha.Text = DateTime.Now.ToString()
                TxtRut.Text = ""
                lector.Close()
                conexion.Close()
                PanelLoading.Visible = False
                ProcesaAsistencia()
            Else
                PanelLoading.Visible = True
                lblCargando.Text = "Usuario no existe / Fechas fuera de intervalo. Reintente."
                ProgressBarCargando.Style = ProgressBarStyle.Continuous
                ProgressBarCargando.Value = 100
                ' MsgBox("Usuario no existe")
                conexion.Close()
                TxtRut.Text = ""
                lector.Close()
            End If
        Catch ex As Exception
            conexion.Close()
            TxtRut.Text = ""
            lector.Close()
            ProcesaDatosOffline()
            lblmensaje.Text = ex.Message.ToString
            'MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub ProcesaDatosOffline()
        'Procesa los datos de manera offline con un archivo de LocalDB
        Dim sConnectionString As String = sConnectionLocalHost
        Dim Conn As New OleDbConnection(sConnectionString)

        SQL = " SELECT T_EMPLEADO.COD_INTERNO, T_EMPLEADO.NOMBRES, T_EMPLEADO.APE_PAT, T_EMPLEADO.APE_MAT,P_EMPRESA.RAZON_SOCIAL, T_EMPLEADO.FOTO " &
              " From T_EMPLEADO LEFT OUTER Join  " &
              " P_EMPRESA On T_EMPLEADO.COD_EMPRESA = P_EMPRESA.COD_EMPRESA  " &
              " Where (RIGHT(T_EMPLEADO.TARJETA_NUMERO, 8) =  RIGHT('000'+'" + TxtRut.Text.ToString + "',8)) OR (RIGHT(T_EMPLEADO.RUT, 8) =  RIGHT('000'+'" + TxtRut.Text.ToString + "',8)) AND GETDATE() BETWEEN DATEADD(d,0,DATEDIFF(d,0,T_EMPLEADO.FECHA_CONTRATO)) AND T_EMPLEADO.FECHA_FIN_CONTRATO"

        'SQL = "SELECT T_EMPLEADO.COD_INTERNO, T_EMPLEADO.NOMBRES, T_EMPLEADO.APE_PAT, T_EMPLEADO.APE_MAT,P_EMPRESA.RAZON_SOCIAL " &
        '      " From T_EMPLEADO LEFT OUTER Join  " &
        '      " P_EMPRESA On T_EMPLEADO.COD_EMPRESA = P_EMPRESA.COD_EMPRESA  " &
        '      " Where (Right(T_EMPLEADO.TARJETA_NUMERO, 8) =  '" + TxtRut.Text.ToString + "') AND GETDATE() BETWEEN T_EMPLEADO.FECHA_CONTRATO AND T_EMPLEADO.FECHA_FIN_CONTRATO"

        Dim comandoOffline As New OleDbCommand(SQL, Conn)
        'comandoOffline.Parameters.AddWithValue("@Tarjeta", OleDbType.Char).Value = TxtRut.Text

        Try
            Conn.Open()
            lectorOffline = comandoOffline.ExecuteReader

            If lectorOffline.Read Then
                CodInterno = lectorOffline(0)
                Nombres = lectorOffline(1)
                APE_PAT = lectorOffline(2)
                APE_MAT = lectorOffline(3)
                Empresa = lectorOffline(4)
                trabajadorExiste = "S"

                lblNombre.Text = lectorOffline(1) & " " & lectorOffline(2) & " " & lectorOffline(3)
                lblEmpresa.Text = Empresa
                lblFecha.Text = Now.ToString
                lectorOffline.Close()
                TxtRut.Text = ""
                Conn.Close()
                PanelLoading.Visible = False
                ProcesaAsistenciaOffline()
            Else
                PanelLoading.Visible = True
                lblCargando.Text = "Usuario no existe / Fechas fuera de intervalo. Reintente."
                ProgressBarCargando.Style = ProgressBarStyle.Continuous
                ProgressBarCargando.Value = 100
                ' MsgBox("Usuario no existe")
                Conn.Close()
                TxtRut.Text = ""
                lectorOffline.Close()
            End If
        Catch ex As Exception
            lblmensaje.Text = ex.Message.ToString
            'MsgBox(ex.Message.ToString)
            Conn.Close()
            TxtRut.Text = ""
            lector.Close()
        End Try
    End Sub

End Class
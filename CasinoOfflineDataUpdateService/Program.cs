﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using ApiBUK;
using System.Net.NetworkInformation;
using System.Data.SqlClient;

namespace CasinoOfflineDataUpdateService
{
    class Program
    {
        public static string urlAPI = "https://app.ctrlit.cl/ctrl/api/registrosPorRecinto?id=q9n7zM3bgi&dia=13-09-2022";

        public static string idPlanta = "";
        public static string ipServerDevStar = "10.2.0.148";
        public static Ping serviceChecker = new Ping();
        public static int timeOutPing = 40000;
        public static bool onlineCloud = false;
        //public static string strConnectCloud = @"Data Source=10.2.0.148\DEVSTAR;Initial Catalog=BUK;User ID=sa;Password=L30$2Kv.Tv112.c;";
        public static string strConnectCloud = @"Data Source=10.2.0.116\CULLULFE_II;Initial Catalog=BD_FORESTALLEONERA;User ID=softwares;Password=asist$loga@27";
        public static string strConnectLocal = @"Data Source=.\SQLEXPRESS;Initial Catalog=BD_FORESTALLEONERA;Integrated Security=True;";
        public static string authTokenAPI = "5c5JLMyWphHT9bn6i4HbYaEH";
        public static DateTime lastRecordAt;
        static void Main(string[] args)
        {

            Console.WriteLine("SERVICIO FANTASMA DE ACTUALIZACIÓN DATOS CASINO");
            Console.WriteLine("====================================================================================");
            Console.WriteLine("Área Transformación Digital, Grupo Leonera.");
            Console.WriteLine("2022 Saúl Candia.");
            Console.WriteLine("====================================================================================");

            DateTime fecha = DateTime.Now;
            string logTexto = "";

            logTexto = DateTime.Now.ToString() + " Servicio inicializado, comenzando...";
            WriteLog(logTexto);
            Console.WriteLine(logTexto);


            System.Threading.Thread.Sleep(1000);
            //Actualiza dotación        

            logTexto = DateTime.Now.ToString() + " LIMPIANDO TABLA TRABAJADORES";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);
            cleanAllData();

            logTexto = DateTime.Now.ToString() + " RECARGA NOMINA DE TRABAJADORES LOCAL";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);
            procesaDatosBUKPersonasLOCAL();

            logTexto = DateTime.Now.ToString() + " RECARGA NOMINA DE TRABAJADORES EN CLOUD";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);
            procesaDatosBUKPersonasCLOUD();


            logTexto = DateTime.Now.ToString() + " ENVIO A CLOUD COLACIONES GENERADAS EN MODO OFFLINE";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);
            procesaDatosOfflineCasino();

            logTexto = DateTime.Now.ToString() + " ENVIO A BD COLACIONES GENERADAS EN MODO OFFLINE";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);
            eliminaDatosOfflineCasino();

            logTexto = DateTime.Now.ToString() + " SERVICIO SE CERRARÁ EN CINCO SEGUNDOS";
            Console.WriteLine(logTexto);
            WriteLog(logTexto);

            System.Threading.Thread.Sleep(5000);

            logTexto = DateTime.Now.ToString() + " NOS VEMOS :-)";
            WriteLog(logTexto);
            Environment.Exit(0);
        }

        public static async Task<string> GetHttp(string urlAPI)
        {
            WebRequest oRequest = WebRequest.Create(urlAPI);
            WebResponse oRespuesta = oRequest.GetResponse();
            StreamReader sr = new StreamReader(oRespuesta.GetResponseStream());
            return await sr.ReadToEndAsync();
        }

        public static void cleanAllData()
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(strConnectLocal))
                {

                    string logTexto = DateTime.Now.ToString() + " COMENZANDO LIMPIEZA EN BD LOCALHOST";
                    Console.WriteLine(logTexto);
                    string queryString_3 = "DELETE FROM T_EMPLEADO WHERE RUT NOT IN (SELECT RUT FROM [T_EMPLEADO] WHERE RUT IN ('000000000','555555','111111111','200000000')) AND COD_EMPRESA NOT IN(SELECT COD_EMPRESA FROM[T_EMPLEADO] WHERE COD_EMPRESA IN('EXTE', 'EXT'))";
                    SqlCommand command_3 = new SqlCommand(queryString_3, conexion);
                    command_3.Connection.Open();
                    command_3.ExecuteNonQuery();
                    command_3.Connection.Close();
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception EX)
            {
                string logTexto = DateTime.Now.ToString() + " NO SE PUDO LIMPIAR TABLA YA QUE NO HAY CONEXION O BASE DE DATOS EN LOCALHOST.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                logTexto = DateTime.Now.ToString() + " "+EX.Message.ToString();
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                //environment.exit(0);
            }

            try
            {
                using (SqlConnection conexion = new SqlConnection(strConnectCloud))
                {

                    string logTexto = DateTime.Now.ToString() + " COMENZANDO LIMPIEZA EN BD EN CLOUD...";
                    Console.WriteLine(logTexto);
                    string queryString_3 = "DELETE FROM T_EMPLEADO WHERE RUT NOT IN (SELECT RUT FROM [T_EMPLEADO] WHERE RUT IN ('000000000','555555','111111111','200000000')) AND COD_EMPRESA NOT IN(SELECT COD_EMPRESA FROM[T_EMPLEADO] WHERE COD_EMPRESA IN('EXTE', 'EXT'))";
                    SqlCommand command_3 = new SqlCommand(queryString_3, conexion);
                    command_3.Connection.Open();
                    command_3.ExecuteNonQuery();
                    command_3.Connection.Close();
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception EX)
            {
                string logTexto = DateTime.Now.ToString() + " NO SE PUDO LIMPIAR TABLA YA QUE NO HAY CONEXION CON CLOUD.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                logTexto = DateTime.Now.ToString() + " " + EX.Message.ToString();
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                //environment.exit(0);
            }
        }

        public static void procesaDatosBUKPersonasCLOUD()
        {
            //BD CLOUD
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send("8.8.8.8", 2000);
                if (reply.Address != null)
                {
                    Console.WriteLine("Status :  " + reply.Status + " \n Time : " + reply.RoundtripTime.ToString() + " \n Address : " + reply.Address);
                    //Console.WriteLine(reply.ToString());


                    string logTexto = "";
                    string apiUri = "https://leonera.buk.cl/api/v1/chile/employees?status=activo&page_size=100";
                    string respuesta = "";

                    //Llama a API BUK
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUri);
                    request.Accept = "*/*";
                    request.Headers.Add("auth_token", authTokenAPI);
                    request.Method = "GET";
                    request.Timeout = timeOutPing;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            throw new Exception("Error code " + response.StatusCode);
                            logTexto = DateTime.Now.ToString() + " ERROR: " + response.StatusCode;
                            WriteLog(logTexto);
                        }

                        // Process the response stream
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                            {
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    respuesta = reader.ReadToEnd();
                                }
                            }
                        } // End of 

                        TrabajadorClass.Root nominaTrabajador = JsonConvert.DeserializeObject<TrabajadorClass.Root>(respuesta);

                        for (int b = 1; b <= nominaTrabajador.pagination.total_pages; b++)
                        {
                            logTexto = DateTime.Now.ToString() + " API PÁGINA: " + b.ToString() + " de " + nominaTrabajador.pagination.total_pages.ToString();
                            WriteLog(logTexto);
                            Console.WriteLine(logTexto);
                            if (b == 1)
                            {
                                for (int c = 0; c < 100; c++)
                                {
                                    using (SqlConnection conexion = new SqlConnection(strConnectCloud))
                                    {
                                        using (SqlCommand cmd = new SqlCommand("INSERT_EMPLEADOS_ACTIVOS_FROM_BUK", conexion))
                                        {
                                            cmd.CommandType = CommandType.StoredProcedure;

                                            cmd.Parameters.Add("@RUT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].rut;
                                            cmd.Parameters.Add("@NOMBRES", SqlDbType.VarChar).Value = nominaTrabajador.data[c].first_name;
                                            cmd.Parameters.Add("@ACTIVE_SINCE", SqlDbType.VarChar).Value = nominaTrabajador.data[c].active_since;

                                            //VALIDA LOS NULOS ANTES DE ENTRAR LOS DATOS

                                            if (nominaTrabajador.data[c].surname is null)
                                            {
                                                cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = "";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].surname;
                                            }

                                            if (nominaTrabajador.data[c].second_surname is null)
                                            {
                                                cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = "";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].second_surname;
                                            }

                                            if (nominaTrabajador.data[c].active_until is null)
                                            {
                                                cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = "20901231";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = nominaTrabajador.data[c].active_until;
                                            }

                                            if (nominaTrabajador.data[c].current_job.company_id is null)
                                            {
                                                cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = "1";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = nominaTrabajador.data[c].current_job.company_id;
                                            }


                                            cmd.Parameters.Add("@FECHA_NACIMIENTO", SqlDbType.VarChar).Value = nominaTrabajador.data[c].birthday;
                                            conexion.Open();
                                            cmd.ExecuteNonQuery();
                                            conexion.Close();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                apiUri = "https://leonera.buk.cl/api/v1/chile/employees?status=activo&page_size=100&page=" + b.ToString();
                                //Llama a otra API BUK
                                request = (HttpWebRequest)WebRequest.Create(apiUri);
                                request.Accept = "*/*";
                                request.Headers.Add("auth_token", authTokenAPI);
                                request.Timeout = timeOutPing;

                                using (HttpWebResponse response2 = (HttpWebResponse)request.GetResponse())
                                {

                                    if (response2.StatusCode != HttpStatusCode.OK)
                                    {
                                        throw new Exception("Error code" + response2.StatusCode);
                                        logTexto = DateTime.Now.ToString() + " ERROR: " + response2.StatusCode;
                                        WriteLog(logTexto);
                                    }

                                    // Process the response stream
                                    using (Stream responseStream2 = response2.GetResponseStream())
                                    {
                                        if (responseStream2 != null)
                                        {
                                            using (StreamReader reader = new StreamReader(responseStream2))
                                            {
                                                respuesta = reader.ReadToEnd();
                                            }
                                        }
                                    } // End of 

                                    TrabajadorClass.Root nominaTrabajador2 = JsonConvert.DeserializeObject<TrabajadorClass.Root>(respuesta);

                                    for (int c = 0; c < nominaTrabajador2.data.Count; c++)
                                    {
                                        logTexto = DateTime.Now.ToString() + " API PÁGINA: " + c.ToString() + " de " + nominaTrabajador2.pagination.total_pages.ToString();
                                        WriteLog(logTexto);
                                        using (SqlConnection conexion = new SqlConnection(strConnectCloud))
                                        {
                                            using (SqlCommand cmd = new SqlCommand("INSERT_EMPLEADOS_ACTIVOS_FROM_BUK", conexion))
                                            {
                                                cmd.CommandType = CommandType.StoredProcedure;

                                                cmd.Parameters.Add("@RUT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].rut;
                                                cmd.Parameters.Add("@NOMBRES", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].first_name;
                                                cmd.Parameters.Add("@ACTIVE_SINCE", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].active_since;

                                                //VALIDA LOS NULOS ANTES DE ENTRAR LOS DATOS

                                                if (nominaTrabajador2.data[c].surname is null)
                                                {
                                                    cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = "";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].surname;
                                                }

                                                if (nominaTrabajador2.data[c].second_surname is null)
                                                {
                                                    cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = "";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].second_surname;
                                                }

                                                if (nominaTrabajador2.data[c].active_until is null)
                                                {
                                                    cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = "20901231";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].active_until;
                                                }

                                                if (nominaTrabajador2.data[c].current_job.company_id is null)
                                                {
                                                    cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = "1";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = nominaTrabajador2.data[c].current_job.company_id;
                                                }


                                                cmd.Parameters.Add("@FECHA_NACIMIENTO", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].birthday;
                                                conexion.Open();
                                                cmd.ExecuteNonQuery();
                                                conexion.Close();
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch (Exception EX)
            {
                Console.WriteLine("ERROR: No hay PING al servicio.");
                string logTexto = DateTime.Now.ToString() + " ERROR: No hay PING al servicio BUK.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                logTexto = DateTime.Now.ToString() + " NO SE PUDO SINCRONIZAR LA DATA DE EMPLEADOS EN BD CLOUD.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                //environment.exit(0);
            }
        }

        public static void procesaDatosBUKPersonasLOCAL()
        {
            //LOCALHOST
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send("8.8.8.8", 2000);
                if (reply.Address != null)
                {
                    Console.WriteLine("Status :  " + reply.Status + " \n Time : " + reply.RoundtripTime.ToString() + " \n Address : " + reply.Address);
                    //Console.WriteLine(reply.ToString());


                    string logTexto = "";
                    string apiUri = "https://leonera.buk.cl/api/v1/chile/employees?status=activo&page_size=100";
                    string respuesta = "";

                    //Llama a API BUK
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUri);
                    request.Accept = "*/*";
                    request.Headers.Add("auth_token", authTokenAPI);
                    request.Method = "GET";
                    request.Timeout = timeOutPing;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            throw new Exception("Error code " + response.StatusCode);
                            logTexto = DateTime.Now.ToString() + " ERROR: " + response.StatusCode;
                            WriteLog(logTexto);
                        }

                        // Process the response stream
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                            {
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    respuesta = reader.ReadToEnd();
                                }
                            }
                        } // End of 

                        TrabajadorClass.Root nominaTrabajador = JsonConvert.DeserializeObject<TrabajadorClass.Root>(respuesta);

                        for (int b = 1; b <= nominaTrabajador.pagination.total_pages; b++)
                        {
                            logTexto = DateTime.Now.ToString() + " API PÁGINA: " + b.ToString() + " de " + nominaTrabajador.pagination.total_pages.ToString();
                            WriteLog(logTexto);
                            Console.WriteLine(logTexto);
                            if (b == 1)
                            {
                                for (int c = 0; c < 100; c++)
                                {
                                    
                                    using (SqlConnection conexion = new SqlConnection(strConnectLocal))
                                    {
                                        using (SqlCommand cmd = new SqlCommand("INSERT_EMPLEADOS_ACTIVOS_FROM_BUK", conexion))
                                        {
                                            cmd.CommandType = CommandType.StoredProcedure;

                                            cmd.Parameters.Add("@RUT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].rut;
                                            cmd.Parameters.Add("@NOMBRES", SqlDbType.VarChar).Value = nominaTrabajador.data[c].first_name;
                                            cmd.Parameters.Add("@ACTIVE_SINCE", SqlDbType.VarChar).Value = nominaTrabajador.data[c].active_since;

                                            //VALIDA LOS NULOS ANTES DE ENTRAR LOS DATOS

                                            if (nominaTrabajador.data[c].surname is null)
                                            {
                                                cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = "";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].surname;
                                            }

                                            if (nominaTrabajador.data[c].current_job.company_id is null)
                                            {
                                                cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = "1";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = nominaTrabajador.data[c].current_job.company_id;
                                            }

                                            if (nominaTrabajador.data[c].second_surname is null)
                                            {
                                                cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = "";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = nominaTrabajador.data[c].second_surname;
                                            }

                                            if (nominaTrabajador.data[c].active_until is null)
                                            {
                                                cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = "20901231";
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = nominaTrabajador.data[c].active_until;
                                            }


                                            cmd.Parameters.Add("@FECHA_NACIMIENTO", SqlDbType.VarChar).Value = nominaTrabajador.data[c].birthday;
                                            conexion.Open();
                                            cmd.ExecuteNonQuery();
                                            conexion.Close();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                apiUri = "https://leonera.buk.cl/api/v1/chile/employees?status=activo&page_size=100&page=" + b.ToString();
                                //Llama a otra API BUK
                                request = (HttpWebRequest)WebRequest.Create(apiUri);
                                request.Accept = "*/*";
                                request.Headers.Add("auth_token", authTokenAPI);
                                request.Timeout = timeOutPing;

                                using (HttpWebResponse response2 = (HttpWebResponse)request.GetResponse())
                                {

                                    if (response2.StatusCode != HttpStatusCode.OK)
                                    {
                                        throw new Exception("Error code" + response2.StatusCode);
                                        logTexto = DateTime.Now.ToString() + " ERROR: " + response2.StatusCode;
                                        WriteLog(logTexto);
                                    }

                                    // Process the response stream
                                    using (Stream responseStream2 = response2.GetResponseStream())
                                    {
                                        if (responseStream2 != null)
                                        {
                                            using (StreamReader reader = new StreamReader(responseStream2))
                                            {
                                                respuesta = reader.ReadToEnd();
                                            }
                                        }
                                    } // End of 

                                    TrabajadorClass.Root nominaTrabajador2 = JsonConvert.DeserializeObject<TrabajadorClass.Root>(respuesta);

                                    for (int c = 0; c < nominaTrabajador2.data.Count; c++)
                                    {
                                        using (SqlConnection conexion = new SqlConnection(strConnectLocal))
                                        {
                                            using (SqlCommand cmd = new SqlCommand("INSERT_EMPLEADOS_ACTIVOS_FROM_BUK", conexion))
                                            {
                                                cmd.CommandType = CommandType.StoredProcedure;

                                                cmd.Parameters.Add("@RUT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].rut;
                                                cmd.Parameters.Add("@NOMBRES", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].first_name;
                                                cmd.Parameters.Add("@ACTIVE_SINCE", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].active_since;

                                                //VALIDA LOS NULOS ANTES DE ENTRAR LOS DATOS

                                                if (nominaTrabajador2.data[c].surname is null)
                                                {
                                                    cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = "";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@APE_PAT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].surname;
                                                }

                                                if (nominaTrabajador2.data[c].current_job.company_id is null)
                                                {
                                                    cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = "1";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@ID_EMPRESA_BUK", SqlDbType.Float).Value = nominaTrabajador2.data[c].current_job.company_id;
                                                }

                                                if (nominaTrabajador2.data[c].second_surname is null)
                                                {
                                                    cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = "";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@APE_MAT", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].second_surname;
                                                }

                                                if (nominaTrabajador2.data[c].active_until is null)
                                                {
                                                    cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = "20901231";
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Add("@ACTIVE_UNTIL", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].active_until;
                                                }


                                                cmd.Parameters.Add("@FECHA_NACIMIENTO", SqlDbType.VarChar).Value = nominaTrabajador2.data[c].birthday;
                                                conexion.Open();
                                                cmd.ExecuteNonQuery();
                                                conexion.Close();
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch (Exception EX)
            {
                Console.WriteLine("ERROR: No hay PING al servicio.");
                string logTexto = DateTime.Now.ToString() + " ERROR: No hay PING al servicio BUK.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                logTexto = DateTime.Now.ToString() + " NO SE PUDO SINCRONIZAR LA DATA DE EMPLEADOS EN BD LOCAL.";
                Console.WriteLine(logTexto);
                WriteLog(logTexto);
                //environment.exit(0);
            }
        }




        public static void procesaDatosOfflineCasino()
        {
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send("8.8.8.8", 2000);
                if (reply.Address != null)
                {
                    SqlConnection con = new SqlConnection(strConnectCloud);
                    using (SqlCommand command = new SqlCommand("SELECT TOP 1 MAX(FECHACOLACION) FROM REGISTROCASINOS", con))
                    {
                        con.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            lastRecordAt = reader.GetDateTime(0);
                        }
                        con.Close();
                    }

                    string year = lastRecordAt.Year.ToString();
                    string month = lastRecordAt.Month.ToString();
                    string day = lastRecordAt.Day.ToString();
                    string hour = lastRecordAt.Hour.ToString();
                    string minute = lastRecordAt.Minute.ToString();
                    string second = "00";
                    string dateTimeFormateado = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + second + ".000";

                    //'2011-04-12T00:00:00.000'

                    SqlConnection conLocal = new SqlConnection(strConnectLocal);
                    string SQL = "SELECT idColacion, COD_INTERNO, FechaColacion, TipoColacion, TipoCasino FROM REGISTROCASINOS WHERE FECHACOLACION >= '" + dateTimeFormateado + "' AND FECHACOLACION <= GETDATE()";
                    using (SqlCommand command = new SqlCommand(SQL, conLocal))
                    {
                        conLocal.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            int idColacion = reader.GetInt32(0);
                            string COD_INTERNO = reader.GetString(1);
                            DateTime FechaColacion = reader.GetDateTime(2);
                            string tipoColacion = reader.GetString(3);
                            string TipoCasino = reader.GetString(4);

                            string SQL2 = "INSERT INTO [REGISTROCASINOS]([COD_INTERNO],[FechaColacion],[TipoColacion],[TipoCasino]) VALUES ('" + COD_INTERNO + "',CONVERT(DATETIME,'" + FechaColacion + "'),'" + tipoColacion + "','" + TipoCasino + "')";

                            using (SqlConnection conexion = new SqlConnection(strConnectCloud))
                            {
                                using (SqlCommand cmd = new SqlCommand(SQL2, conexion))
                                {
                                    conexion.Open();
                                    cmd.ExecuteNonQuery();
                                    conexion.Close();
                                }
                            }
                        }
                        conLocal.Close();
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch
            {
                Console.WriteLine("ERROR: No hay PING al servicio.");
                string logTexto = DateTime.Now.ToString() + " ERROR: No hay PING al servicio.";
                Console.WriteLine(logTexto);
                logTexto = DateTime.Now.ToString() + " NO SE PUDO SINCRONIZAR LA DATA DE OFFLINE DEL CASINO.";
                Console.WriteLine(logTexto);
                //environment.exit(0);
            }
        }

        public static void eliminaDatosOfflineCasino()
        {
            using (SqlConnection conexion = new SqlConnection(strConnectLocal))
            {

                string logTexto = DateTime.Now.ToString() + " LIMPIANDO TABLA REGISTROCASINOS";
                Console.WriteLine(logTexto);
                string queryString_3 = "TRUNCATE TABLE REGISTROCASINOS";
                SqlCommand command_3 = new SqlCommand(queryString_3, conexion);
                command_3.Connection.Open();
                command_3.ExecuteNonQuery();
                command_3.Connection.Close();
                System.Threading.Thread.Sleep(1000);
                logTexto = DateTime.Now.ToString() + " SINCRONIZACIÓN COMPLETADA CON ÉXITO";
                Console.WriteLine(logTexto);
                //environment.exit(0);
            }
        }

        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = "C:\\Logs\\";
            logFilePath = logFilePath + "UpdateFromBUKService-Log_" + System.DateTime.Today.ToString("dd-MM-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }

    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiBUK
{
    class TrabajadorClass
    {
        public class Boss
        {
            public int? id { get; set; }
            public string rut { get; set; }
        }

        public class CurrentJob
        {
            public string periodicity { get; set; }
            public string frequency { get; set; }
            public string working_schedule_type { get; set; }
            public bool zone_assignment { get; set; }
            public object union { get; set; }
            public int? id { get; set; }
            public int? company_id { get; set; }
            public int? area_id { get; set; }
            public string contract_type { get; set; }
            public string start_date { get; set; }
            public string end_date { get; set; }
            public string contract_finishing_date_1 { get; set; }
            public string contract_finishing_date_2 { get; set; }
            public double weekly_hours { get; set; }
            public string cost_center { get; set; }
            public string active_until { get; set; }
            public string termination_reason { get; set; }
            public object location_id { get; set; }
            public bool without_wage { get; set; }
            public Boss boss { get; set; }
            public Role role { get; set; }
            public CustomAttributes custom_attributes { get; set; }
        }

        public class CustomAttributes
        {
            [JsonProperty("Jornada Excepcional- Turnos")]
            public object JornadaExcepcionalTurnos { get; set; }

            [JsonProperty("Turnos de Lunes a Sábado")]
            public object TurnosDeLunesASábado { get; set; }

            [JsonProperty("Turnos de Lunes a Viernes")]
            public object TurnosDeLunesAViernes { get; set; }
            public string ctrlit_recinto { get; set; }

            [JsonProperty("Item De Gasto")]
            public string ItemDeGasto { get; set; }

            [JsonProperty("Nombre del item de Gasto")]
            public string NombreDelItemDeGasto { get; set; }
            public string Sección { get; set; }

            [JsonProperty("Sección 2")]
            public string Sección2 { get; set; }
        }

        public class Datum
        {
            public int? person_id { get; set; }
            public int? id { get; set; }
            public string picture_url { get; set; }
            public string first_name { get; set; }
            public string surname { get; set; }
            public string second_surname { get; set; }
            public string full_name { get; set; }
            public string rut { get; set; }
            public string code_sheet { get; set; }
            public string email { get; set; }
            public string personal_email { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string district { get; set; }
            public int? location_id { get; set; }
            public string region { get; set; }
            public string office_phone { get; set; }
            public string phone { get; set; }
            public string gender { get; set; }
            public string birthday { get; set; }
            public string university { get; set; }
            public string degree { get; set; }
            public string active_since { get; set; }
            public string status { get; set; }
            public string payment_method { get; set; }
            public string bank { get; set; }
            public string account_type { get; set; }
            public string account_number { get; set; }
            public bool private_role { get; set; }
            public string progressive_vacations_start { get; set; }
            public string nationality { get; set; }
            public string country_code { get; set; }
            public string civil_status { get; set; }
            public string health_company { get; set; }
            public string pension_regime { get; set; }
            public string pension_fund { get; set; }
            public string afc { get; set; }
            public CustomAttributes custom_attributes { get; set; }
            public string active_until { get; set; }
            public string termination_reason { get; set; }
            public CurrentJob current_job { get; set; }
            public List<FamilyResponsability> family_responsabilities { get; set; }

            public List<Job> jobs { get; set; }
        }

        public class Job
        {
            public int? id { get; set; }
            public int? company_id { get; set; }
            public int? area_id { get; set; }
            public string contract_type { get; set; }
            public string start_date { get; set; }
            public string end_date { get; set; }
            public string contract_finishing_date_1 { get; set; }
            public string contract_finishing_date_2 { get; set; }
            public double weekly_hours { get; set; }
            public string cost_center { get; set; }
            public string active_until { get; set; }
            public string termination_reason { get; set; }
            public string periodicity { get; set; }
            public string frequency { get; set; }
            public string working_schedule_type { get; set; }
            public object location_id { get; set; }
            public bool without_wage { get; set; }
            public bool zone_assignment { get; set; }
            public Boss boss { get; set; }
            public CustomAttributes custom_attributes { get; set; }
        }


        public class FamilyResponsability
        {
            public int? id { get; set; }
            public string family_allowance_section { get; set; }
            public int? simple_family_responsability { get; set; }
            public int? maternity_family_responsability { get; set; }
            public int? invalid_family_responsability { get; set; }
            public string start_date { get; set; }
            public string end_date { get; set; }
            public List<object> responsability_details { get; set; }
        }

        public class Pagination
        {
            public string next { get; set; }
            public object previous { get; set; }
            public int count { get; set; }
            public int total_pages { get; set; }
        }

        public class Role
        {
            public int? id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public string requirements { get; set; }
            public List<int> area_ids { get; set; }
            public object role_family { get; set; }
        }

        public class Root
        {
            public Pagination pagination { get; set; }
            public List<Datum> data { get; set; }
        }



















        //public List<oDataTrabajador> data  { get; set; }





        //public string status { get; set; }

        //public class oDataTrabajador
        //{
        //    public string person_id { get; set; }

        //    public string rut { get; set; }
        //    public string full_name { get; set; }
        //    public string status { get; set; }
        //    public string nationality { get; set; }
        //    public string gender { get; set; }

        //    public List<oDataJobsClass> current_job { get; set; }
        //}


        //public class oDataJobsClass
        //{
        //    [JsonProperty("current_job")]
        //    public string working_schedule_type { get; set; }

        //    public string start_date { get; set; }
        //    public string contract_type { get; set; }

        //    public List<oDataRoleClass> role { get; set; }
        //}


        //public class oDataRoleClass
        //{
        //    [JsonProperty("role")]
        //    public string name { get; set; }

        //}



        ////public string nombre { get; set; }

        ////public string sentido { get; set; }

        ////public bool entrada { get; set; }

        ////public string ingreso { get; set; }
        ////public string salida { get; set; }

        ////public string rutEmpleador { get; set; }
    }
}

﻿Imports Microsoft.Web.WebView2.Core
Imports Microsoft.Web.WebView2.WinForms

Public Class Estadisticas
    Private Sub txtURL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtURL.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            Try
                Me.WebView21.Source = New Uri(txtURL.Text)
                ''OR
                'Me.WebView21.CoreWebView2.Navigate(txtURL.Text)



            Catch ex As UriFormatException
                MessageBox.Show("URL Completa con HTTP(S) Incluido")
            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try
        End If

    End Sub

    Private Sub Estadisticas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WebView21.EnsureCoreWebView2Async()
        'txtURL.Text = "https://www.elantro.cl"
        txtURL.Text = "https://app.powerbi.com/reportEmbed?reportId=f8373a0f-2a86-472b-86d6-8fecc5bb8904&autoAuth=true&ctid=70de5114-582f-4f85-a1a5-b2bf39704f0a&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXBhYXMtMS1zY3VzLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9"
        'Me.WebView21.CoreWebView2.NavigateToString(txtURL.Text)
        Me.WebView21.Source = New Uri(txtURL.Text)
    End Sub

    Private Sub btnMinimizeForm_Click(sender As Object, e As EventArgs) Handles btnMinimizeForm.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btnRefreshForm_Click(sender As Object, e As EventArgs) Handles btnRefreshForm.Click
        Me.WebView21.Reload()
    End Sub

    Private Sub btnCloseForm_Click(sender As Object, e As EventArgs) Handles btnCloseForm.Click
        Me.Close()
        VisorCasino.Show()
    End Sub
End Class
﻿Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient

Public Class spalsh

    Private Sub spalsh_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Opacity = 1
        Me.Visible = True
        Timer1.Start()
        lblMensaje.Visible = True
        btnReconectar.Visible = False
        Btn_Offline.Visible = False
        ModoOffline = False
    End Sub

    Private Sub conectar() Handles Me.Activated
        Dim control1
        lblMensaje.Text = "Probando conexión a base de datos..."
        Try


            'If My.Computer.Network.Ping("10.2.0.111") Then
            '    Dim conexion As New SqlConnection("Data Source=10.2.0.91, 1433\LOGOS;Initial Catalog=BD_FORESTALLEONERA_LOGAM;User ID=softwares;Password=asist$loga@27;connection timeout =30")
            If My.Computer.Network.Ping("10.2.0.116") Then
                Dim conexion As New SqlConnection("Data Source=10.2.0.116\CULLULFE_II;Initial Catalog=BD_FORESTALLEONERA;User ID=softwares;Password=asist$loga@27;connection timeout =30")

                conexion.Open()
                lblMensaje.Text = "Conexión detectada"
                conexion.Close()
                Me.WindowState = FormWindowState.Minimized
                Me.Visible = False
                control1 = True
                ModoOffline = False
                SelectorCasino.Show()
            Else
                'lblMensaje.Text = "Conexión local no encontrada, buscando conexión en la nube"
                'control1 = False

                'Try
                '    Dim conexion As New SqlConnection("Data Source=186.10.19.202, 41001\LOGOS, 1433;Initial Catalog=BD_FORESTALLEONERA_LOGAM;User ID=softwares;Password=asist$loga@27;connection timeout = 20")
                '    conexion.Open()
                '    lblMensaje.Text = "Conexión encontrada en la nube"
                '    conexion.Close()
                '    Me.WindowState = FormWindowState.Minimized
                '    Me.Visible = False
                '    control2 = True
                '    ModoOffline = False
                '    SelectorCasino.Show()
                'Catch ex As Exception
                '    lblMensaje.Text = "Conexión en la nube no encontrada"
                '    lblException.Text = ex.Message.ToString()
                '    control2 = False
                'End Try
                lblMensaje.Text = "Conexión a red e internet no encontrado, Verifique y vuelva a intentarlo."
                btnReconectar.Visible = True
                Btn_Offline.Visible = True
                Me.UseWaitCursor = False
            End If
        Catch ex As Exception
            lblMensaje.Text = "Conexión a red e internet no encontrado, Verifique y vuelva a intentarlo."
            btnReconectar.Visible = True
            Btn_Offline.Visible = True
            Me.UseWaitCursor = False
        End Try

        'If control1 = False And control2 = False Then
        '    lblMensaje.Text = "Conexión a red e internet no encontrado, Verifique y vuelva a intentarlo."
        '    btnReconectar.Visible = True
        '    Btn_Offline.Visible = True
        '    Me.UseWaitCursor = False
        '    Exit Sub
        'End If
    End Sub


    Private Sub btnReconectar_Click(sender As Object, e As EventArgs) Handles btnReconectar.Click
        lblMensaje.Text = ""
        btnReconectar.Visible = True
        Me.UseWaitCursor = True
        Btn_Offline.Visible = False
        conectar()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If (MsgBox("¿Esta seguro de salir?", vbQuestion + vbYesNo) = vbYes) Then
            Application.Exit()
        End If

    End Sub

    Private Sub Btn_Offline_Click(sender As Object, e As EventArgs) Handles Btn_Offline.Click
        Dim confirmarOffline As DialogResult
        confirmarOffline = MessageBox.Show("Use este modo SOLAMENTE si tiene problemas para conectar a internet. ¿Continuar?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If confirmarOffline = vbYes Then
            Me.WindowState = FormWindowState.Minimized
            Me.Visible = False
            ModoOffline = True
            SelectorCasino.Show()
        End If
    End Sub
End Class
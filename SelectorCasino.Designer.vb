﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorCasino
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectorCasino))
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.btnCasinoPaneles = New Bunifu.Framework.UI.BunifuImageButton()
        Me.btnCasinoBatuco = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.btnCasinoValdivia = New Bunifu.Framework.UI.BunifuImageButton()
        CType(Me.btnCasinoPaneles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCasinoBatuco, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCasinoValdivia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 5
        Me.BunifuElipse1.TargetControl = Me
        '
        'btnCasinoPaneles
        '
        Me.btnCasinoPaneles.BackColor = System.Drawing.Color.White
        Me.btnCasinoPaneles.Image = CType(resources.GetObject("btnCasinoPaneles.Image"), System.Drawing.Image)
        Me.btnCasinoPaneles.ImageActive = Nothing
        Me.btnCasinoPaneles.Location = New System.Drawing.Point(12, 71)
        Me.btnCasinoPaneles.Name = "btnCasinoPaneles"
        Me.btnCasinoPaneles.Size = New System.Drawing.Size(214, 214)
        Me.btnCasinoPaneles.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCasinoPaneles.TabIndex = 0
        Me.btnCasinoPaneles.TabStop = False
        Me.btnCasinoPaneles.Zoom = 10
        '
        'btnCasinoBatuco
        '
        Me.btnCasinoBatuco.BackColor = System.Drawing.Color.White
        Me.btnCasinoBatuco.Image = CType(resources.GetObject("btnCasinoBatuco.Image"), System.Drawing.Image)
        Me.btnCasinoBatuco.ImageActive = Nothing
        Me.btnCasinoBatuco.Location = New System.Drawing.Point(294, 71)
        Me.btnCasinoBatuco.Name = "btnCasinoBatuco"
        Me.btnCasinoBatuco.Size = New System.Drawing.Size(214, 214)
        Me.btnCasinoBatuco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCasinoBatuco.TabIndex = 1
        Me.btnCasinoBatuco.TabStop = False
        Me.btnCasinoBatuco.Zoom = 10
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Font = New System.Drawing.Font("Segoe UI Semibold", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel3.ForeColor = System.Drawing.Color.Navy
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(60, 298)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(110, 64)
        Me.BunifuCustomLabel3.TabIndex = 6
        Me.BunifuCustomLabel3.Text = "Casino" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PANELES"
        Me.BunifuCustomLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Segoe UI Semibold", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.Color.Navy
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(344, 298)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(107, 64)
        Me.BunifuCustomLabel1.TabIndex = 7
        Me.BunifuCustomLabel1.Text = "Casino" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BATUCO"
        Me.BunifuCustomLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Font = New System.Drawing.Font("Segoe UI", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel2.ForeColor = System.Drawing.Color.Navy
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(231, 9)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(362, 50)
        Me.BunifuCustomLabel2.TabIndex = 8
        Me.BunifuCustomLabel2.Text = "ESCOJA SU CASINO"
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Segoe UI Semibold", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.ForeColor = System.Drawing.Color.Navy
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(623, 298)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(119, 64)
        Me.BunifuCustomLabel4.TabIndex = 10
        Me.BunifuCustomLabel4.Text = "Casino" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "VALDIVIA"
        Me.BunifuCustomLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCasinoValdivia
        '
        Me.btnCasinoValdivia.BackColor = System.Drawing.Color.White
        Me.btnCasinoValdivia.Image = CType(resources.GetObject("btnCasinoValdivia.Image"), System.Drawing.Image)
        Me.btnCasinoValdivia.ImageActive = Nothing
        Me.btnCasinoValdivia.Location = New System.Drawing.Point(574, 71)
        Me.btnCasinoValdivia.Name = "btnCasinoValdivia"
        Me.btnCasinoValdivia.Size = New System.Drawing.Size(214, 214)
        Me.btnCasinoValdivia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCasinoValdivia.TabIndex = 9
        Me.btnCasinoValdivia.TabStop = False
        Me.btnCasinoValdivia.Zoom = 10
        '
        'SelectorCasino
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(800, 379)
        Me.Controls.Add(Me.BunifuCustomLabel4)
        Me.Controls.Add(Me.btnCasinoValdivia)
        Me.Controls.Add(Me.BunifuCustomLabel2)
        Me.Controls.Add(Me.BunifuCustomLabel1)
        Me.Controls.Add(Me.BunifuCustomLabel3)
        Me.Controls.Add(Me.btnCasinoBatuco)
        Me.Controls.Add(Me.btnCasinoPaneles)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SelectorCasino"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SelectorCasino"
        Me.TopMost = True
        CType(Me.btnCasinoPaneles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCasinoBatuco, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCasinoValdivia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents btnCasinoPaneles As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents btnCasinoBatuco As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents btnCasinoValdivia As Bunifu.Framework.UI.BunifuImageButton
End Class

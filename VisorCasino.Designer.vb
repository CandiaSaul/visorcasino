﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorCasino
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VisorCasino))
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.panelTop = New System.Windows.Forms.Panel()
        Me.lblCasinoIniciado = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel5 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuContextual = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.chk_ModoOffline = New System.Windows.Forms.ToolStripMenuItem()
        Me.CtxMinimizar = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnRestartApp = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CtxImprimirResumen = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpcionesDeEquipoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnConectarRed = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnReiniciar = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnOSK = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelIzq = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.ProgressBarOtros = New Bunifu.Framework.UI.BunifuProgressBar()
        Me.lblCuentaOtros = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ProgressBarHolding = New Bunifu.Framework.UI.BunifuProgressBar()
        Me.lblCuentaHolding = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.lblTotal = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ProgressBarTransportes = New Bunifu.Framework.UI.BunifuProgressBar()
        Me.lblCuentaTransportes = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ProgressBarPaneles = New Bunifu.Framework.UI.BunifuProgressBar()
        Me.ProgressBarForestal = New Bunifu.Framework.UI.BunifuProgressBar()
        Me.lblCuentaForestal = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.lblCuentaPaneles = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.panelFoot = New System.Windows.Forms.Panel()
        Me.panelContenido = New System.Windows.Forms.Panel()
        Me.PanelLoading = New System.Windows.Forms.Panel()
        Me.ProgressBarCargando = New System.Windows.Forms.ProgressBar()
        Me.lblCargando = New System.Windows.Forms.Label()
        Me.lblmensaje2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.lblmensaje = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel6 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.TxtRut = New System.Windows.Forms.TextBox()
        Me.lblFecha = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.pbLogoTicket = New System.Windows.Forms.PictureBox()
        Me.lblNombre = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.lblEmpresa = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.HojaImpresion = New System.Drawing.Printing.PrintDocument()
        Me.ImpresionResumen = New System.Drawing.Printing.PrintDocument()
        Me.panelTop.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuContextual.SuspendLayout()
        Me.panelIzq.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.panelContenido.SuspendLayout()
        Me.PanelLoading.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.pbLogoTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 25
        Me.BunifuElipse1.TargetControl = Me
        '
        'panelTop
        '
        Me.panelTop.BackColor = System.Drawing.Color.MidnightBlue
        Me.panelTop.Controls.Add(Me.lblCasinoIniciado)
        Me.panelTop.Controls.Add(Me.BunifuCustomLabel5)
        Me.panelTop.Controls.Add(Me.PictureBox1)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(1366, 122)
        Me.panelTop.TabIndex = 0
        '
        'lblCasinoIniciado
        '
        Me.lblCasinoIniciado.AutoSize = True
        Me.lblCasinoIniciado.Font = New System.Drawing.Font("Segoe UI", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasinoIniciado.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCasinoIniciado.Location = New System.Drawing.Point(239, 78)
        Me.lblCasinoIniciado.Name = "lblCasinoIniciado"
        Me.lblCasinoIniciado.Size = New System.Drawing.Size(330, 32)
        Me.lblCasinoIniciado.TabIndex = 6
        Me.lblCasinoIniciado.Text = "Versión 2.1 - Grupo Leonera"
        '
        'BunifuCustomLabel5
        '
        Me.BunifuCustomLabel5.AutoSize = True
        Me.BunifuCustomLabel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuCustomLabel5.Font = New System.Drawing.Font("Segoe UI", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel5.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.BunifuCustomLabel5.Location = New System.Drawing.Point(232, 0)
        Me.BunifuCustomLabel5.Name = "BunifuCustomLabel5"
        Me.BunifuCustomLabel5.Size = New System.Drawing.Size(907, 86)
        Me.BunifuCustomLabel5.TabIndex = 5
        Me.BunifuCustomLabel5.Text = "Sistema de gestión de casino"
        Me.BunifuCustomLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.ContextMenuStrip = Me.MenuContextual
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(232, 122)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'MenuContextual
        '
        Me.MenuContextual.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.chk_ModoOffline, Me.CtxMinimizar, Me.BtnRestartApp, Me.ToolStripSeparator1, Me.CtxImprimirResumen, Me.OpcionesDeEquipoToolStripMenuItem, Me.btnReports})
        Me.MenuContextual.Name = "MenuContextual"
        Me.MenuContextual.Size = New System.Drawing.Size(235, 142)
        '
        'chk_ModoOffline
        '
        Me.chk_ModoOffline.Checked = True
        Me.chk_ModoOffline.CheckOnClick = True
        Me.chk_ModoOffline.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chk_ModoOffline.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_ModoOffline.Name = "chk_ModoOffline"
        Me.chk_ModoOffline.Size = New System.Drawing.Size(234, 22)
        Me.chk_ModoOffline.Text = "Modo Sin Conexión (OffLine)"
        '
        'CtxMinimizar
        '
        Me.CtxMinimizar.Image = Global.VisorCasinoV2.My.Resources.Resources.minimize
        Me.CtxMinimizar.Name = "CtxMinimizar"
        Me.CtxMinimizar.Size = New System.Drawing.Size(234, 22)
        Me.CtxMinimizar.Text = "Minimizar aplicación"
        '
        'BtnRestartApp
        '
        Me.BtnRestartApp.Name = "BtnRestartApp"
        Me.BtnRestartApp.Size = New System.Drawing.Size(234, 22)
        Me.BtnRestartApp.Text = "Reiniciar aplicación Casino"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(231, 6)
        '
        'CtxImprimirResumen
        '
        Me.CtxImprimirResumen.Image = Global.VisorCasinoV2.My.Resources.Resources.ic_print_black_48dp_1x
        Me.CtxImprimirResumen.Name = "CtxImprimirResumen"
        Me.CtxImprimirResumen.Size = New System.Drawing.Size(234, 22)
        Me.CtxImprimirResumen.Text = "Imprimir resumen"
        '
        'OpcionesDeEquipoToolStripMenuItem
        '
        Me.OpcionesDeEquipoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BtnConectarRed, Me.BtnReiniciar, Me.BtnOSK})
        Me.OpcionesDeEquipoToolStripMenuItem.Name = "OpcionesDeEquipoToolStripMenuItem"
        Me.OpcionesDeEquipoToolStripMenuItem.Size = New System.Drawing.Size(234, 22)
        Me.OpcionesDeEquipoToolStripMenuItem.Text = "Opciones de equipo"
        '
        'BtnConectarRed
        '
        Me.BtnConectarRed.Image = CType(resources.GetObject("BtnConectarRed.Image"), System.Drawing.Image)
        Me.BtnConectarRed.Name = "BtnConectarRed"
        Me.BtnConectarRed.Size = New System.Drawing.Size(175, 22)
        Me.BtnConectarRed.Text = "Conectar a otra red"
        '
        'BtnReiniciar
        '
        Me.BtnReiniciar.Image = CType(resources.GetObject("BtnReiniciar.Image"), System.Drawing.Image)
        Me.BtnReiniciar.Name = "BtnReiniciar"
        Me.BtnReiniciar.Size = New System.Drawing.Size(175, 22)
        Me.BtnReiniciar.Text = "Reiniciar equipo"
        '
        'BtnOSK
        '
        Me.BtnOSK.Image = CType(resources.GetObject("BtnOSK.Image"), System.Drawing.Image)
        Me.BtnOSK.Name = "BtnOSK"
        Me.BtnOSK.Size = New System.Drawing.Size(175, 22)
        Me.BtnOSK.Text = "Teclado en pantalla"
        '
        'btnReports
        '
        Me.btnReports.Image = CType(resources.GetObject("btnReports.Image"), System.Drawing.Image)
        Me.btnReports.Name = "btnReports"
        Me.btnReports.Size = New System.Drawing.Size(234, 22)
        Me.btnReports.Text = "Visualizar reporte con PowerBI"
        '
        'panelIzq
        '
        Me.panelIzq.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.panelIzq.Controls.Add(Me.Panel3)
        Me.panelIzq.Controls.Add(Me.ProgressBarOtros)
        Me.panelIzq.Controls.Add(Me.lblCuentaOtros)
        Me.panelIzq.Controls.Add(Me.ProgressBarHolding)
        Me.panelIzq.Controls.Add(Me.lblCuentaHolding)
        Me.panelIzq.Controls.Add(Me.BunifuCustomLabel4)
        Me.panelIzq.Controls.Add(Me.lblTotal)
        Me.panelIzq.Controls.Add(Me.ProgressBarTransportes)
        Me.panelIzq.Controls.Add(Me.lblCuentaTransportes)
        Me.panelIzq.Controls.Add(Me.ProgressBarPaneles)
        Me.panelIzq.Controls.Add(Me.ProgressBarForestal)
        Me.panelIzq.Controls.Add(Me.lblCuentaForestal)
        Me.panelIzq.Controls.Add(Me.lblCuentaPaneles)
        Me.panelIzq.Dock = System.Windows.Forms.DockStyle.Left
        Me.panelIzq.Location = New System.Drawing.Point(0, 122)
        Me.panelIzq.Name = "panelIzq"
        Me.panelIzq.Size = New System.Drawing.Size(371, 646)
        Me.panelIzq.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblStatus)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 590)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(371, 56)
        Me.Panel3.TabIndex = 33
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(3, 13)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(51, 21)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Modo"
        '
        'ProgressBarOtros
        '
        Me.ProgressBarOtros.BackColor = System.Drawing.Color.Transparent
        Me.ProgressBarOtros.BorderRadius = 5
        Me.ProgressBarOtros.Location = New System.Drawing.Point(17, 396)
        Me.ProgressBarOtros.MaximumValue = 100
        Me.ProgressBarOtros.Name = "ProgressBarOtros"
        Me.ProgressBarOtros.ProgressColor = System.Drawing.Color.Firebrick
        Me.ProgressBarOtros.Size = New System.Drawing.Size(292, 16)
        Me.ProgressBarOtros.TabIndex = 32
        Me.ProgressBarOtros.Value = 0
        '
        'lblCuentaOtros
        '
        Me.lblCuentaOtros.AutoSize = True
        Me.lblCuentaOtros.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaOtros.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCuentaOtros.Location = New System.Drawing.Point(12, 363)
        Me.lblCuentaOtros.Name = "lblCuentaOtros"
        Me.lblCuentaOtros.Size = New System.Drawing.Size(69, 30)
        Me.lblCuentaOtros.TabIndex = 31
        Me.lblCuentaOtros.Text = "Otros:"
        '
        'ProgressBarHolding
        '
        Me.ProgressBarHolding.BackColor = System.Drawing.Color.Transparent
        Me.ProgressBarHolding.BorderRadius = 5
        Me.ProgressBarHolding.Location = New System.Drawing.Point(17, 132)
        Me.ProgressBarHolding.MaximumValue = 100
        Me.ProgressBarHolding.Name = "ProgressBarHolding"
        Me.ProgressBarHolding.ProgressColor = System.Drawing.Color.Firebrick
        Me.ProgressBarHolding.Size = New System.Drawing.Size(292, 16)
        Me.ProgressBarHolding.TabIndex = 29
        Me.ProgressBarHolding.Value = 0
        '
        'lblCuentaHolding
        '
        Me.lblCuentaHolding.AutoSize = True
        Me.lblCuentaHolding.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaHolding.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCuentaHolding.Location = New System.Drawing.Point(12, 90)
        Me.lblCuentaHolding.Name = "lblCuentaHolding"
        Me.lblCuentaHolding.Size = New System.Drawing.Size(91, 30)
        Me.lblCuentaHolding.TabIndex = 30
        Me.lblCuentaHolding.Text = "Holding:"
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Segoe UI Semibold", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.ForeColor = System.Drawing.Color.DarkBlue
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(75, 25)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(225, 45)
        Me.BunifuCustomLabel4.TabIndex = 21
        Me.BunifuCustomLabel4.Text = "ESTADÍSTICAS"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblTotal.Location = New System.Drawing.Point(12, 446)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(62, 30)
        Me.lblTotal.TabIndex = 28
        Me.lblTotal.Text = "Total:"
        '
        'ProgressBarTransportes
        '
        Me.ProgressBarTransportes.BackColor = System.Drawing.Color.Transparent
        Me.ProgressBarTransportes.BorderRadius = 5
        Me.ProgressBarTransportes.Location = New System.Drawing.Point(17, 328)
        Me.ProgressBarTransportes.MaximumValue = 100
        Me.ProgressBarTransportes.Name = "ProgressBarTransportes"
        Me.ProgressBarTransportes.ProgressColor = System.Drawing.Color.Firebrick
        Me.ProgressBarTransportes.Size = New System.Drawing.Size(292, 16)
        Me.ProgressBarTransportes.TabIndex = 28
        Me.ProgressBarTransportes.Value = 0
        '
        'lblCuentaTransportes
        '
        Me.lblCuentaTransportes.AutoSize = True
        Me.lblCuentaTransportes.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaTransportes.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCuentaTransportes.Location = New System.Drawing.Point(12, 298)
        Me.lblCuentaTransportes.Name = "lblCuentaTransportes"
        Me.lblCuentaTransportes.Size = New System.Drawing.Size(124, 30)
        Me.lblCuentaTransportes.TabIndex = 27
        Me.lblCuentaTransportes.Text = "Transportes:"
        '
        'ProgressBarPaneles
        '
        Me.ProgressBarPaneles.BackColor = System.Drawing.Color.Transparent
        Me.ProgressBarPaneles.BorderRadius = 5
        Me.ProgressBarPaneles.Location = New System.Drawing.Point(17, 264)
        Me.ProgressBarPaneles.MaximumValue = 100
        Me.ProgressBarPaneles.Name = "ProgressBarPaneles"
        Me.ProgressBarPaneles.ProgressColor = System.Drawing.Color.Firebrick
        Me.ProgressBarPaneles.Size = New System.Drawing.Size(292, 16)
        Me.ProgressBarPaneles.TabIndex = 27
        Me.ProgressBarPaneles.Value = 0
        '
        'ProgressBarForestal
        '
        Me.ProgressBarForestal.BackColor = System.Drawing.Color.Transparent
        Me.ProgressBarForestal.BorderRadius = 5
        Me.ProgressBarForestal.Location = New System.Drawing.Point(17, 196)
        Me.ProgressBarForestal.MaximumValue = 100
        Me.ProgressBarForestal.Name = "ProgressBarForestal"
        Me.ProgressBarForestal.ProgressColor = System.Drawing.Color.Firebrick
        Me.ProgressBarForestal.Size = New System.Drawing.Size(292, 16)
        Me.ProgressBarForestal.TabIndex = 0
        Me.ProgressBarForestal.Value = 0
        '
        'lblCuentaForestal
        '
        Me.lblCuentaForestal.AutoSize = True
        Me.lblCuentaForestal.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaForestal.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCuentaForestal.Location = New System.Drawing.Point(12, 163)
        Me.lblCuentaForestal.Name = "lblCuentaForestal"
        Me.lblCuentaForestal.Size = New System.Drawing.Size(90, 30)
        Me.lblCuentaForestal.TabIndex = 25
        Me.lblCuentaForestal.Text = "Forestal:"
        '
        'lblCuentaPaneles
        '
        Me.lblCuentaPaneles.AutoSize = True
        Me.lblCuentaPaneles.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaPaneles.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCuentaPaneles.Location = New System.Drawing.Point(12, 231)
        Me.lblCuentaPaneles.Name = "lblCuentaPaneles"
        Me.lblCuentaPaneles.Size = New System.Drawing.Size(88, 30)
        Me.lblCuentaPaneles.TabIndex = 26
        Me.lblCuentaPaneles.Text = "Paneles:"
        '
        'panelFoot
        '
        Me.panelFoot.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelFoot.Location = New System.Drawing.Point(371, 755)
        Me.panelFoot.Name = "panelFoot"
        Me.panelFoot.Size = New System.Drawing.Size(995, 13)
        Me.panelFoot.TabIndex = 2
        '
        'panelContenido
        '
        Me.panelContenido.Controls.Add(Me.PanelLoading)
        Me.panelContenido.Controls.Add(Me.lblmensaje2)
        Me.panelContenido.Controls.Add(Me.lblmensaje)
        Me.panelContenido.Controls.Add(Me.Panel2)
        Me.panelContenido.Controls.Add(Me.Panel1)
        Me.panelContenido.Controls.Add(Me.lblFecha)
        Me.panelContenido.Controls.Add(Me.pbLogoTicket)
        Me.panelContenido.Controls.Add(Me.lblNombre)
        Me.panelContenido.Controls.Add(Me.lblEmpresa)
        Me.panelContenido.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelContenido.Location = New System.Drawing.Point(371, 122)
        Me.panelContenido.Name = "panelContenido"
        Me.panelContenido.Size = New System.Drawing.Size(995, 633)
        Me.panelContenido.TabIndex = 3
        '
        'PanelLoading
        '
        Me.PanelLoading.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelLoading.Controls.Add(Me.ProgressBarCargando)
        Me.PanelLoading.Controls.Add(Me.lblCargando)
        Me.PanelLoading.Location = New System.Drawing.Point(127, 183)
        Me.PanelLoading.Name = "PanelLoading"
        Me.PanelLoading.Size = New System.Drawing.Size(856, 166)
        Me.PanelLoading.TabIndex = 27
        Me.PanelLoading.UseWaitCursor = True
        '
        'ProgressBarCargando
        '
        Me.ProgressBarCargando.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBarCargando.Location = New System.Drawing.Point(5, 70)
        Me.ProgressBarCargando.Name = "ProgressBarCargando"
        Me.ProgressBarCargando.Size = New System.Drawing.Size(842, 32)
        Me.ProgressBarCargando.Step = 20
        Me.ProgressBarCargando.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.ProgressBarCargando.TabIndex = 0
        Me.ProgressBarCargando.UseWaitCursor = True
        Me.ProgressBarCargando.Value = 80
        '
        'lblCargando
        '
        Me.lblCargando.AutoSize = True
        Me.lblCargando.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCargando.Font = New System.Drawing.Font("Segoe UI", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCargando.Location = New System.Drawing.Point(0, 0)
        Me.lblCargando.Name = "lblCargando"
        Me.lblCargando.Size = New System.Drawing.Size(141, 37)
        Me.lblCargando.TabIndex = 0
        Me.lblCargando.Text = "Cargando"
        Me.lblCargando.UseWaitCursor = True
        '
        'lblmensaje2
        '
        Me.lblmensaje2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblmensaje2.AutoSize = True
        Me.lblmensaje2.Font = New System.Drawing.Font("Segoe UI", 12.75!)
        Me.lblmensaje2.ForeColor = System.Drawing.Color.Red
        Me.lblmensaje2.Location = New System.Drawing.Point(135, 421)
        Me.lblmensaje2.Name = "lblmensaje2"
        Me.lblmensaje2.Size = New System.Drawing.Size(0, 23)
        Me.lblmensaje2.TabIndex = 22
        '
        'lblmensaje
        '
        Me.lblmensaje.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblmensaje.Font = New System.Drawing.Font("Segoe UI", 12.75!)
        Me.lblmensaje.ForeColor = System.Drawing.Color.Red
        Me.lblmensaje.Location = New System.Drawing.Point(135, 392)
        Me.lblmensaje.Name = "lblmensaje"
        Me.lblmensaje.Size = New System.Drawing.Size(860, 29)
        Me.lblmensaje.TabIndex = 21
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.BunifuCustomLabel1)
        Me.Panel2.Controls.Add(Me.BunifuCustomLabel2)
        Me.Panel2.Controls.Add(Me.BunifuCustomLabel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 151)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(128, 482)
        Me.Panel2.TabIndex = 26
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(8, 32)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(108, 32)
        Me.BunifuCustomLabel1.TabIndex = 18
        Me.BunifuCustomLabel1.Text = "Nombre:"
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel2.ForeColor = System.Drawing.Color.DarkBlue
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(6, 97)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(110, 32)
        Me.BunifuCustomLabel2.TabIndex = 19
        Me.BunifuCustomLabel2.Text = "Empresa:"
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel3.ForeColor = System.Drawing.Color.DarkBlue
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(34, 161)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(82, 32)
        Me.BunifuCustomLabel3.TabIndex = 23
        Me.BunifuCustomLabel3.Text = "Fecha:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel6)
        Me.Panel1.Controls.Add(Me.TxtRut)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(995, 151)
        Me.Panel1.TabIndex = 25
        '
        'BunifuCustomLabel6
        '
        Me.BunifuCustomLabel6.AutoSize = True
        Me.BunifuCustomLabel6.Font = New System.Drawing.Font("Segoe UI Semibold", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel6.ForeColor = System.Drawing.Color.DarkBlue
        Me.BunifuCustomLabel6.Location = New System.Drawing.Point(449, 25)
        Me.BunifuCustomLabel6.Name = "BunifuCustomLabel6"
        Me.BunifuCustomLabel6.Size = New System.Drawing.Size(145, 45)
        Me.BunifuCustomLabel6.TabIndex = 20
        Me.BunifuCustomLabel6.Text = "TARJETA"
        '
        'TxtRut
        '
        Me.TxtRut.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
        Me.TxtRut.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtRut.Location = New System.Drawing.Point(364, 85)
        Me.TxtRut.MaxLength = 12
        Me.TxtRut.Name = "TxtRut"
        Me.TxtRut.Size = New System.Drawing.Size(308, 39)
        Me.TxtRut.TabIndex = 1
        Me.TxtRut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TxtRut.WordWrap = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblFecha.Location = New System.Drawing.Point(133, 312)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(75, 32)
        Me.lblFecha.TabIndex = 24
        Me.lblFecha.Text = "Fecha"
        '
        'pbLogoTicket
        '
        Me.pbLogoTicket.Image = CType(resources.GetObject("pbLogoTicket.Image"), System.Drawing.Image)
        Me.pbLogoTicket.Location = New System.Drawing.Point(858, 529)
        Me.pbLogoTicket.Name = "pbLogoTicket"
        Me.pbLogoTicket.Size = New System.Drawing.Size(87, 53)
        Me.pbLogoTicket.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbLogoTicket.TabIndex = 14
        Me.pbLogoTicket.TabStop = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Font = New System.Drawing.Font("Segoe UI", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblNombre.Location = New System.Drawing.Point(133, 183)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(125, 32)
        Me.lblNombre.TabIndex = 16
        Me.lblNombre.Text = "Juan Soto"
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpresa.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblEmpresa.Location = New System.Drawing.Point(130, 248)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(226, 32)
        Me.lblEmpresa.TabIndex = 17
        Me.lblEmpresa.Text = "Transportes Leonera"
        '
        'HojaImpresion
        '
        '
        'ImpresionResumen
        '
        '
        'VisorCasino
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1366, 768)
        Me.Controls.Add(Me.panelContenido)
        Me.Controls.Add(Me.panelFoot)
        Me.Controls.Add(Me.panelIzq)
        Me.Controls.Add(Me.panelTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "VisorCasino"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VisorCasino"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panelTop.ResumeLayout(False)
        Me.panelTop.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuContextual.ResumeLayout(False)
        Me.panelIzq.ResumeLayout(False)
        Me.panelIzq.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.panelContenido.ResumeLayout(False)
        Me.panelContenido.PerformLayout()
        Me.PanelLoading.ResumeLayout(False)
        Me.PanelLoading.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pbLogoTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents panelContenido As Panel
    Friend WithEvents panelFoot As Panel
    Friend WithEvents panelIzq As Panel
    Friend WithEvents panelTop As Panel
    Friend WithEvents lblFecha As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents pbLogoTicket As PictureBox
    Friend WithEvents BunifuCustomLabel6 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblEmpresa As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblNombre As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblCasinoIniciado As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel5 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents HojaImpresion As Printing.PrintDocument
    Friend WithEvents lblTotal As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblCuentaTransportes As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblCuentaPaneles As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblCuentaForestal As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ProgressBarForestal As Bunifu.Framework.UI.BunifuProgressBar
    Friend WithEvents ProgressBarTransportes As Bunifu.Framework.UI.BunifuProgressBar
    Friend WithEvents ProgressBarPaneles As Bunifu.Framework.UI.BunifuProgressBar
    Friend WithEvents TxtRut As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ProgressBarHolding As Bunifu.Framework.UI.BunifuProgressBar
    Friend WithEvents lblCuentaHolding As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ImpresionResumen As Printing.PrintDocument
    Friend WithEvents MenuContextual As ContextMenuStrip
    Friend WithEvents CtxImprimirResumen As ToolStripMenuItem
    Friend WithEvents BtnRestartApp As ToolStripMenuItem
    Friend WithEvents OpcionesDeEquipoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BtnReiniciar As ToolStripMenuItem
    Friend WithEvents BtnConectarRed As ToolStripMenuItem
    Friend WithEvents BtnOSK As ToolStripMenuItem
    Friend WithEvents lblmensaje2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents lblmensaje As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents CtxMinimizar As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ProgressBarOtros As Bunifu.Framework.UI.BunifuProgressBar
    Friend WithEvents lblCuentaOtros As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PanelLoading As Panel
    Friend WithEvents lblCargando As Label
    Friend WithEvents ProgressBarCargando As ProgressBar
    Friend WithEvents Panel3 As Panel
    Friend WithEvents lblStatus As Label
    Friend WithEvents chk_ModoOffline As ToolStripMenuItem
    Friend WithEvents btnReports As ToolStripMenuItem
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Estadisticas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Estadisticas))
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.WebView21 = New Microsoft.Web.WebView2.WinForms.WebView2()
        Me.txtURL = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PanelTop = New System.Windows.Forms.Panel()
        Me.btnRefreshForm = New System.Windows.Forms.Button()
        Me.btnMinimizeForm = New System.Windows.Forms.Button()
        Me.btnCloseForm = New System.Windows.Forms.Button()
        CType(Me.WebView21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.PanelTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 20
        Me.BunifuElipse1.TargetControl = Me
        '
        'WebView21
        '
        Me.WebView21.CreationProperties = Nothing
        Me.WebView21.DefaultBackgroundColor = System.Drawing.Color.White
        Me.WebView21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebView21.Location = New System.Drawing.Point(3, 3)
        Me.WebView21.Name = "WebView21"
        Me.WebView21.Size = New System.Drawing.Size(1354, 626)
        Me.WebView21.TabIndex = 0
        Me.WebView21.ZoomFactor = 1.0R
        '
        'txtURL
        '
        Me.txtURL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtURL.Location = New System.Drawing.Point(3, 6)
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(1354, 20)
        Me.txtURL.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.WebView21, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 64)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.22642!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1360, 632)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.Controls.Add(Me.txtURL)
        Me.Panel3.Location = New System.Drawing.Point(0, 39)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1360, 29)
        Me.Panel3.TabIndex = 4
        '
        'PanelTop
        '
        Me.PanelTop.BackColor = System.Drawing.Color.DarkGreen
        Me.PanelTop.Controls.Add(Me.btnRefreshForm)
        Me.PanelTop.Controls.Add(Me.btnMinimizeForm)
        Me.PanelTop.Controls.Add(Me.btnCloseForm)
        Me.PanelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelTop.Location = New System.Drawing.Point(0, 0)
        Me.PanelTop.Name = "PanelTop"
        Me.PanelTop.Size = New System.Drawing.Size(1360, 44)
        Me.PanelTop.TabIndex = 5
        '
        'btnRefreshForm
        '
        Me.btnRefreshForm.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefreshForm.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnRefreshForm.Image = CType(resources.GetObject("btnRefreshForm.Image"), System.Drawing.Image)
        Me.btnRefreshForm.Location = New System.Drawing.Point(1270, 4)
        Me.btnRefreshForm.Name = "btnRefreshForm"
        Me.btnRefreshForm.Size = New System.Drawing.Size(36, 36)
        Me.btnRefreshForm.TabIndex = 7
        Me.btnRefreshForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRefreshForm.UseVisualStyleBackColor = False
        '
        'btnMinimizeForm
        '
        Me.btnMinimizeForm.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizeForm.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnMinimizeForm.Image = CType(resources.GetObject("btnMinimizeForm.Image"), System.Drawing.Image)
        Me.btnMinimizeForm.Location = New System.Drawing.Point(1228, 4)
        Me.btnMinimizeForm.Name = "btnMinimizeForm"
        Me.btnMinimizeForm.Size = New System.Drawing.Size(36, 36)
        Me.btnMinimizeForm.TabIndex = 6
        Me.btnMinimizeForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMinimizeForm.UseVisualStyleBackColor = False
        Me.btnMinimizeForm.Visible = False
        '
        'btnCloseForm
        '
        Me.btnCloseForm.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCloseForm.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnCloseForm.Image = CType(resources.GetObject("btnCloseForm.Image"), System.Drawing.Image)
        Me.btnCloseForm.Location = New System.Drawing.Point(1312, 4)
        Me.btnCloseForm.Name = "btnCloseForm"
        Me.btnCloseForm.Size = New System.Drawing.Size(36, 36)
        Me.btnCloseForm.TabIndex = 5
        Me.btnCloseForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnCloseForm.UseVisualStyleBackColor = False
        '
        'Estadisticas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1360, 700)
        Me.Controls.Add(Me.PanelTop)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Estadisticas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visor de reportería Casino"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.WebView21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.PanelTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents WebView21 As Microsoft.Web.WebView2.WinForms.WebView2
    Friend WithEvents txtURL As TextBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents PanelTop As Panel
    Friend WithEvents btnCloseForm As Button
    Friend WithEvents btnRefreshForm As Button
    Friend WithEvents btnMinimizeForm As Button
End Class
